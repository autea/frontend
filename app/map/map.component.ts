import { Component } from '@angular/core';
import leaflet from 'leaflet';

@Component({
  selector: 'map',
  templateUrl: 'app/map/map.view.html',
  styleUrls: ['app/map/map.style.css']
})
export class MapComponent {

	constructor() {

	}
	ngOnInit() {
		var osmUrl = "http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";
		var osm = new L.TileLayer(osmUrl);
		var map = L.map('map', {
		    center: [50.3001, 18.6524],
		    zoom: 12,
		    zoomControl: false
		});
		map.addLayer(osm);

		var layerControl = L.control.zoom({'position': 'topright'}).addTo(map);
	}
}
