import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { MapComponent } from './map/map.component';
import { InfoWrapperComponent } from './info-wrapper/info-wrapper.component';
import { MainViewComponent } from './main-view/main-view.component';
import { AppComponent }  from './app.component';

@NgModule({
  imports:      [ BrowserModule ],
  declarations: [ AppComponent, MainViewComponent, MapComponent, InfoWrapperComponent],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
