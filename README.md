# Installation for development environment

- `docker run -d -p 8080:80 -p 3000:3000 --name pure-breath-front -v /path/to/angular/folder/:/var/www/angular hensansi/angular2`
- `docker exec -it pure-breath-front /bin/bash`
- `npm install`
- `npm start`
